require 'HTTParty'
class Document < ActiveRecord::Base

 def self.crawl_documents
    page = HTTParty.get('https://drive.google.com/folderview?id=0B7cCcjXnte7vVktTQ1RfMHNRZGM&usp=drive_web')
    parse_page = Nokogiri::HTML(page)
    documents_array = []
    parse_page.css('div.flip-entry').map do |element|
        title = element.at_css('div.flip-entry-title').text
        thumb = element.at_css("img")['src']
        name = File.basename(title, ".*")
        file_type = File.extname(title)
        self.save_document(name,file_type,thumb)  
    end
  end
    
  def self.save_document(name,file_type,thumb)
        document = self.new
        document.name = name
        document.file_type = file_type
        document.thumb = thumb
        document.save!
  end 
end

