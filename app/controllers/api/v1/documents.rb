module API
  module V1
    class Documents < Grape::API
      include API::V1::Defaults

      resource :documents do
        

        desc 'Return all Documents'
        get '', root: :documents do
          Document.all
        end



      end
    end
  end
end