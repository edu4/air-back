class ApplicationController < ActionController::Base

  before_action :configure_permitted_parameters

  respond_to :json

  protected

end