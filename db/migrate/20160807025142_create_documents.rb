class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.string :file_type
      t.string :thumb
 
      t.timestamps
    end
  end
end
